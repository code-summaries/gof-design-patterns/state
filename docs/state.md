<div align="center">
  <h1>State</h1>
</div>

<div align="center">
  <img src="state_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **State is a behavioral pattern that lets an object alter its behavior when its internal state
changes.**

### Real-World Analogy

_A vending machine._

The vending machine (context) can have different states and behaviors based on its current condition (state).

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

Context (TCPConnection)-defines the interface of interest to clients.-maintains an instance of a ConcreteState subclass
that defines the current state. • State (TCPState)- defines an interface for encapsulating the behavior associated with
a particular state of the Context.STATE 307 • ConcreteState subclasses (TCPEstablished, TCPListen, TCPClosed)- each
subclass implements a behavior associated with a state of the Context.

### Collaborations

...
Context delegates state-specific requests to the current ConcreteState object.
• A context may pass itself as an argument to the State object handling the
request. This lets the State object access the context if necessary.
• Context isthe primary interface for clients.Clients canconfigure a context with
State objects. Once a context is configured, its clients don't have to deal with
the State objects directly.
• EitherContextortheConcreteState subclasses candecide which state succeeds
another and under what circumstances

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

• An object's behavior depends on its state, and it must change its behavior at run-time depending on that state. •
Operations have large, multipart conditional statements that depend on the object's state. This state is usually
represented by one or more enumerated constants. Often, several operations will contain this same conditional structure.
The State pattern puts each branch of the conditional in a separate class. This lets you treat the object's state as an
object in its own right that can vary independently from other objects.

### Motivation

- ...

onsider a class TCPConnection that represents a network connection. A TCPConnection object can be in one of several
different states: Established, Listening,
Closed. When a TCPConnection object receives requests from other objects, it
responds differently depending on its current state. For example, the effect of an
Open request depends on whether the connection isin itsClosed state orits Established state. The State pattern
describes how TCPConnection can exhibit different
behavior in each state.

The key idea in this pattern is to introduce an abstract class called TCPState
to represent the states of the network connection. The TCPState class declares
an interface common to all classes that represent different operational states.
Subclasses ofTCPStateimplement state-specific behavior. For example, the classes
TCPEstablished and TCPClosed implement behavior particular to the Established
and Closed states of TCPConnection.

The class TCPConnection maintains a state object (an instance of a subclass of
TCPState) that represents the current state of the TCP connection. The class
Connection delegates allstate-specific requests to this state object. TCPConnection
uses its TCPState subclass instance to perform operations particular to the state
of the connection.

Whenever the connection changes state, the TCPConnection object changes the
stateobjectit uses.When the connection goes from established to closed,for example, TCPConnection will replace its
TCPEstablished instance with a TCPClosed
instance

---
Use the State pattern when you have an object that behaves differently depending on its current state, the number of
states is enormous, and the state-specific code changes frequently.

The pattern suggests that you extract all state-specific code into a set of distinct classes. As a result, you can add
new states or change existing ones independently of each other, reducing the maintenance cost.

Use the pattern when you have a class polluted with massive conditionals that alter how the class behaves according to
the current values of the class’s fields.

The State pattern lets you extract branches of these conditionals into methods of corresponding state classes. While
doing so, you can also clean temporary fields and helper methods involved in state-specific code out of your main class.

Use State when you have a lot of duplicate code across similar states and transitions of a condition-based state
machine.

The State pattern lets you compose hierarchies of state classes and reduce duplication by extracting common code into
abstract base classes.

### Known Uses

- ...

Workflow Management: In applications where there are multiple states in a workflow (draft, pending approval, approved,
rejected), the State pattern helps manage transitions between these states.

User Interface: A user interface element that behaves differently based on its state. For instance, a button that
changes its behavior (enabled/disabled) or appearance (color, text) based on certain conditions.

Traffic Light System: Modeling a traffic light where the behavior changes depending on the current state (red, green,
yellow).

Game Development: Character behavior in a game might change based on different states such as idle, attacking,
defending, or moving.

Document Processing: Document processing systems where different actions or validations occur based on the state of the
document (draft, final, archived).

Vending Machine: Managing the behavior of a vending machine where available options and actions change based on its
state (idle, money inserted, item selected).

Networking Protocols: Handling different behaviors in networking protocols (TCP/IP states like established, closed,
waiting) based on the current state of the connection.

Order Processing: In e-commerce systems, managing an order's lifecycle (placed, shipped, delivered, cancelled) and
adjusting available actions based on its current state.

Chatbot Behavior: Chatbots might change their behavior or responses based on the conversation context or user input,
transitioning between states like greeting, gathering information, providing solutions, etc.

Task Management: Task or job scheduling systems that change behavior based on the state of tasks (queued, processing,
completed, failed).

Media Player: Modeling the behavior of a media player where different actions are available based on the playback
state (playing, paused, stopped).

Smart Home Devices: Controlling smart home devices (lights, thermostats, etc.) where the behavior changes based on
different modes or states (home, away, sleep).

Battery Management in Devices: Managing the behavior of a device based on battery levels (low battery, charging, fully
charged) to adjust performance or functionalities.

Reservation Systems: Systems handling reservations or bookings that change behavior based on the availability of
resources (available, booked, pending confirmation).

Concurrent Programming: Managing the behavior of concurrent systems where different actions or strategies are employed
based on the state of concurrent processes.

javax.faces.lifecycle.LifeCycle#execute() (controlled by FacesServlet, the behaviour is dependent on current phase (
state) of JSF lifecycle)

Johnson and Zweig [JZ91] characterize the State pattern and its application to TCP
connection protocols.
Most popular interactive drawing programs provide "tools" for performing op-
erations by direct manipulation. For example, a line-drawing tool lets a user click
and drag to create a new line. A selection tool lets the user select shapes. There's
usually a palette of such tools to choose from. The user thinks of this activity as
picking up a tool and wielding it, but in reality the editor's behavior changes
with the current tool: When a drawing tool is active we create shapes; when the
selection tool is active we select shapes; and so forth. We can use the State pattern
to change the editor's behavior depending on the current tool.
We can define an abstract Tool class from which to define subclasses that imple-
ment tool-specific behavior. The drawing editor maintains a current Tool object
and delegates requests to it. It replaces this object when the user chooses a new
tool, causing the behavior of the drawing editor to change accordingly.
This technique is used in both the HotDraw [Joh92] and Unidraw [VL90] drawing
editor frameworks. It allows clients to define new kinds of tools easily. In HotDraw,
the DrawingController class forwards the requests to the current Tool object. In
Unidraw, the corresponding classes are Viewer and Tool. The following class
diagram sketches the Tool and DrawingController interfaces:
Coplien's Envelope-Letter idiom [Cop92] is related to State. Envelope-Letter is
a technique for changing an object's class at run-time. The State pattern is more
specific, focusing on how to deal with an object whose behavior depends on its
state

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- States of an object.

### Solution to causes of redesign

None (known)

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The State pattern has the following consequences:

1. It localizes state-specific behavior and partitions behavior for different states. The
   State pattern puts all behavior associated with a particular state into one
   object. Because allstate-specific code livesin a State subclass, new states and
   transitions can be added easily by defining new subclasses.
   An alternative is to use data values to define internal states and have Context operations check the data
   explicitly.But then we'd have look-alike conditional or case statements scattered throughout Context's
   implementation.
   Adding a new state could require changing several operations, which complicatesmaintenance.
   The State pattern avoids this problem but might introduce another, because
   the pattern distributes behavior for different states across several State subclasses.This increasesthe number of
   classes and isless compact than a single
   class. But such distribution is actually good if there are many states, which
   would otherwise necessitate large conditional statements.
   Like long procedures, large conditional statements are undesirable. They're
   monolithic and tend to make the code less explicit, which in turn makes
   them difficult to modify and extend. The State pattern offers a better way to
   structure state-specific code. The logic that determines the state transitions
   doesn't reside in monolithic if or switch statements but instead is partitioned between the State subclasses.
   Encapsulating each state transition and
   action in a class elevates the idea of an execution state to full object status.
   That imposes structure on the code and makesits intent clearer.
2. It makes state transitions explicit. When an object defines its current state solely
   in terms of internal data values, its state transitions have no explicit representation; they only show up as
   assignments to some variables. Introducing separate objects for different states makes the transitions more
   explicit. Also, State objects can protect the Context from inconsistent internal states,
   because state transitions are atomic from the Context's perspective—they
   happen by rebinding one variable (the Context's State object variable), not
   several [dCLF93].
3. State objects can beshared. If State objects have no instance variables—that is,
   the state they represent is encoded entirely in their type—then contexts can
   share a State object. When states are shared in this way, they are essentially
   flyweights (see Flyweight (195)) with no intrinsic state, only behavior

Single Responsibility Principle. Organize the code related to particular states into separate classes.
Open/Closed Principle. Introduce new states without changing existing state classes or the context.
Simplify the code of the context by eliminating bulky state machine conditionals.

Applying the pattern can be overkill if a state machine has only a few states or rarely changes.

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_

- ...

The Flyweight (195) pattern explains when and how State objects can be shared.
State objects are often Singletons (127).

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

State pattern can be implemented by methods that change their behavior depending on the objects’ state, controlled
externally.

(recognizable by behavioral methods which changes its behaviour depending on the instance's state which can be
controlled externally)

### Structure

```mermaid
classDiagram
    class Context {
        - state: State
        + setState(State): void
        + request(): void
    }

    class State {
        <<interface>>
        + handle(): void
    }

    class ConcreteState {
        + handle(): void
    }

    Context o-- State: aggregated by
    State <|.. ConcreteState: implements
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

The State pattern has the following consequences:

1. It localizes state-specific behavior and partitions behavior for different states. The
   State pattern puts all behavior associated with a particular state into one
   object. Because all state-specific code lives in a State subclass, new states and
   transitions can be added easily by defining new subclasses.
   An alternative is to use data values to define internal states and have Con-
   text operations check the data explicitly. But then we'd have look-alike con-
   ditional or case statements scattered throughout Context's implementation.
   Adding a new state could require changing several operations, which com-
   plicates maintenance.
   The State pattern avoids this problem but might introduce another, because
   the pattern distributes behavior for different states across several State sub-
   classes. This increases the number of classes and is less compact than a single
   class. But such distribution is actually good if there are many states, which
   would otherwise necessitate large conditional statements.
   Like long procedures, large conditional statements are undesirable. They're
   monolithic and tend to make the code less explicit, which in turn makes
   them difficult to modify and extend. The State pattern offers a better way to
   structure state-specific code. The logic that determines the state transitions
   doesn't reside in monolithic if or switch statements but instead is parti-
   tioned between the State subclasses. Encapsulating each state transition and
   action in a class elevates the idea of an execution state to full object status.
   That imposes structure on the code and makes its intent clearer.
2. It makes state transitions explicit. When an object defines its current state solely
   in terms of internal data values, its state transitions have no explicit repre-
   sentation; they only show up as assignments to some variables. Introduc-
   ing separate objects for different states makes the transitions more explicit. Also, State objects can protect the
   Context from inconsistent internal states,
   because state transitions are atomic from the Context's perspective—they
   happen by rebinding one variable (the Context's State object variable), not
   several [dCLF93].
3. State objects can be shared. If State objects have no instance variables—that is,
   the state they represent is encoded entirely in their type—then contexts can
   share a State object. When states are shared in this way, they are essentially
   flyweights (see Flyweight (195)) with no intrinsic state, only behavior.

In the GumballMachine, the states decide what the next state should be. Do the ConcreteStates always decide what state
to go to next?
No, not always. The alternative is to let the Context decide on the flow of state transitions
As a general guideline,
when the state transitions are fixed they are appropriate for putting in the Context;
however, when the transitions are more dynamic, they are typically placed in the state classes themselves (for instance,
in the GumballMachine the choice of the transition to NoQuarter or SoldOut depended on the runtime count of gumballs)
The disadvantage of having state transitions in the state classes is that we create dependencies between the state
classes.
We tried to minimize this by using getter methods on the Context, rather than hardcoding explicit concrete state
classes.
Notice that by making this decision, you are making a decision as to which classes are closed for modification — the
Context or the state classes

### Implementation

https://youtu.be/jv1-ohCWbE0?si=4bRAdA4JBubj0kvv&t=2493
https://www.youtube.com/watch?v=5OzLrbk82zY

workshop status

In the example we apply the state pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - State](https://refactoring.guru/design-patterns/state)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [DDD Europe: Extreme Modelling Patterns - Alberto Brandolini](https://youtu.be/jv1-ohCWbE0?si=4bRAdA4JBubj0kvv&t=2493)
- [ArjanCodes: The State Design Pattern in Python Explained](https://youtu.be/5OzLrbk82zY?si=CWd16dYxkgsXhitr)
- [Geekific: The State Pattern Explained and Implemented in Java](https://youtu.be/abX4xzaAsoc?si=LFqhytPIh0aLLngt)

<br>
<br>
